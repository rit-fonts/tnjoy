# Build the opentype font from SFD/Feature sources
#
# Copyright 2019-2021 Rajeesh KV <rajeeshknambiar@gmail.com>
# This file is licensed under GPLv3.
#
# SFD files are placed in 'sources' directory, 
# feature files in 'features', build artifacts will
# be in 'build' and test files in 'tests'

PY:=/usr/bin/python3
SHELL:=/bin/bash
SFD_FILES=sources/*.sfd
DOC_FILES=meta/{67-tn-joy-fonts.conf,in.org.rachana.tn-joy.metainfo.xml} README.md LICENSE.txt CHANGELOG.md
BUILD_DIR:=build
FONT_DIR:=fonts
FEATURE_FILE:=features/*-feature.fea
FEATURE_DIR:=features
BUILD_SCRIPT=build/buildfont.py
export FTYPE:=otf
#TTF_FILES=$(addsuffix .ttf, $(addprefix $(FONT_DIR)/, $(basename $(notdir $(SFD_FILES)))))
TEST_DIR=tests
TEST_FILE_SHAPING=$(TEST_DIR)/shaping.txt
TEST_FILE_KERNING=$(TEST_DIR)/kerning.txt
export VERSION:=1.6.2

all: ttf otf woff2 test

# For each SFD file in, find the font style and apply corresponding feature file
# Naming of SFD files should follow "Font-Name-Style" where "Style" is converted to
# lowercase and "style-feature.fea" is applied.
fbuild: $(SFD_FILES) $(FEATURE_FILE)
	rm -rf $(FONT_DIR)/$(FTYPE); mkdir -p $(FONT_DIR)/$(FTYPE)
	for sfd_file in $(SFD_FILES); do \
		font_style=`basename $${sfd_file} '.sfd'`; \
		font_style=`echo $${font_style##*-} | tr '[:upper:]' '[:lower:]'`; \
		$(PY) $(BUILD_SCRIPT) -s $${sfd_file} -f $(FEATURE_DIR)/$${font_style}-feature.fea -v $(VERSION) -t $(FTYPE) -o $(FONT_DIR)/$(FTYPE) ; \
	done

#Create source package for distro
dist:
	fname=`basename $$(echo $(SFD_FILES) | cut -d' ' -f1) .sfd`; \
	fname=`echo $${fname%-*}`; \
	git config tar.tar.xz.command "xz -c"; \
	git archive --prefix=$${fname}/ -o $(FONT_DIR)/$${fname}-source-$(VERSION).tar.xz $(VERSION)

#Usage: make release
release: clean otf ttf woff2
	cp $(DOC_FILES) $(FONT_DIR); 
	#zipping is handled in archive target

#Usage: make beta
beta: VERSION=20
beta: release

archive: 
	#Ugh... this cries for a better method to find family name (remove '-Regular' from RIT-Rachana-Regular)
	fname=`basename $$(echo $(SFD_FILES) | cut -d' ' -f1) .sfd`; \
	fname=`echo $${fname%-*}`; \
	zip -jr ${FONT_DIR}/$${fname}-$(VERSION).zip $(FONT_DIR)

# Generate TTF/OTF/WOFF/WOFF2 all using the same target; but sub-make needs the 
# export+override and -e trickery to pass target-specific variables
# Usage: make ttf; make otf; make woff; make woff2
ttf otf woff woff2: override FTYPE=$@
ttf otf woff woff2:
	@echo $(FTYPE)
	@echo $(VERSION)
	$(MAKE) -e fbuild

#Usage: make test shaping; make test kerning
ifeq (test, $(firstword $(MAKECMDGOALS)))
TEST_TYPE := $(lastword $(MAKECMDGOALS))
$(eval $(TEST_TYPE):;@:)
endif

test: $(FONT_DIR)/otf/*.otf
	if [[ "kerning" == "$(TEST_TYPE)" ]]; then \
		TEST_FILE=$(TEST_FILE_KERNING); \
	elif [[ "shaping" == "$(TEST_TYPE)" ]]; then \
		TEST_FILE=$(TEST_FILE_SHAPING); \
	else \
		python3 $(TEST_DIR)/glyph_without_shaping.py $(FONT_DIR)/otf/*.otf; \
		exit $$?; \
	fi; \
	if [[ -n $${TEST_FILE} ]]; then \
		for f_file in $(FONT_DIR)/otf/*.otf; do \
			font_name=`basename $${f_file} .otf`; \
			hb-view --font-file=$${f_file} --text-file=$${TEST_FILE} -o $(TEST_DIR)/$${font_name}.pdf && \
			xdg-open $(TEST_DIR)/$${font_name}.pdf; \
		done;\
	fi

pdf: $(FONT_DIR)/otf/*.otf
	cd $(TEST_DIR) && xelatex human-rights.tex && \
		xdg-open human-rights.pdf

clean:
	@rm -rf $(TEST_DIR)/*.{aux,log,pdf} $(FONT_DIR)
